import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:provider_app/models/order.dart';
import 'package:provider_app/models/staff.dart';
import 'package:provider_app/screens/auth/reset_password_screen.dart';
import 'package:provider_app/screens/auth/signin_screen.dart';
import 'package:provider_app/screens/auth/signup_screen.dart';
import 'package:go_router/go_router.dart';
import 'package:provider_app/screens/home/assign_driver_screen.dart';
import 'package:provider_app/screens/home/home_screen.dart';
import 'package:provider_app/screens/home/order_details_screen.dart';
import 'package:provider_app/screens/home/past_orders_screen.dart';
import 'package:provider_app/screens/home/unassigned_screen.dart';
import 'package:provider_app/screens/services/services_screen.dart';
import 'package:provider_app/screens/splash_screen.dart';
import 'package:provider_app/screens/staff/staff_profile_screen.dart';
import 'package:provider_app/screens/staff/staff_screen.dart';
import 'package:provider_app/screens/widgets/custom_bottomnav.dart';

final routes = [
  GoRoute(
    path: '/splash',
    name: 'Splash',
    builder: (context, state) => const SplashScreen(),
  ),
  GoRoute(
    path: '/',
    name: 'Home',
    builder: (context, state) => const HomeScreen(),
    routes: [
      GoRoute(
        path: 'assign',
        name: 'AssignOrder',
        builder: (context, state) {
          final order = state.extra as Order;
          return AssignOrderScreen(order: order);
        },
      ),
      GoRoute(
        path: 'unassigned',
        name: 'Unassigned',
        builder: (context, state) => const UnassignedOrdersScreen(),
      ),
      GoRoute(
        path: 'orders',
        name: 'PastOrders',
        builder: (context, state) => const PastOrdersScreen(),
        routes: [
          GoRoute(
            path: 'details',
            name: 'OrderDetails',
            builder: (context, state) => const OrderDetailsScreen(),
          ),
        ],
      ),
      GoRoute(
        path: 'staff',
        name: 'Staff',
        builder: (context, state) => const StaffScreen(),
        routes: [
          GoRoute(
            path: 'profile',
            name: 'StaffProfile',
            builder: (context, state) {
              final staff = state.extra as Staff;
              return StaffProfileScreen(staff: staff);
            },
          ),
        ],
      ),
      GoRoute(
        path: 'services',
        name: 'Services',
        builder: (context, state) => const ServicesScreen(),
      ),
    ],
  ),
  GoRoute(
    path: '/signin',
    name: 'SignIn',
    builder: (context, state) => const SigninScreen(),
  ),
  GoRoute(
    path: '/signup',
    name: 'SignUp',
    builder: (context, state) => const SignupScreen(),
  ),
  GoRoute(
    path: '/reset',
    name: 'ResetPassword',
    builder: (context, state) => const ResetPasswordScreen(),
  ),
];

final router = GoRouter(
  routes: routes,
  initialLocation: '/splash',
  debugLogDiagnostics: true,
  navigatorBuilder: _navigatorBuilder,
);

Navigator _navigatorBuilder(
  BuildContext context,
  GoRouterState state,
  Widget child,
) {
  final excludedPages = [
    // '/',
    '/splash',
    '/signin',
    '/signup',
    '/resetpassword',
    // '/staff',
    // '/services'
  ];

  final isExcluded = excludedPages.contains(state.location);

  return Navigator(
    onPopPage: (route, result) => route.didPop(result),
    pages: [
      if (isExcluded)
        MaterialPage(
          child: Scaffold(
            body: KeyboardDismisser(child: child),
          ),
        )
      else
        MaterialPage(
          child: Scaffold(
            body: KeyboardDismisser(child: child),
            bottomNavigationBar: const CustomBottomNav(),
          ),
        )
    ],
  );
}
