import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/apis/staff_api.dart';
import 'package:provider_app/models/staff.dart';

class StaffRepository {
  final _userApi = StaffApi();

  Stream<QuerySnapshot<Staff>> getStaff() {
    return _userApi.getStaffStream();
  }

  Future<void> createStaff(Map<String, dynamic> staffData) async {
    await _userApi.createStaffMember(Staff.fromJson(staffData));
  }

  Future<void> updateStaff(String staffId, Map<String, dynamic> data) async {
    await _userApi.updateStaffMember(staffId, data);
  }

  Future<void> deleteStaff(String staffId) async {
    await _userApi.deleteStaffMember(staffId);
  }
}
