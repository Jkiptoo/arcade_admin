import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/apis/auth_api.dart';
import 'package:provider_app/apis/user_api.dart';
import 'package:provider_app/models/user.dart';

class UserRepository {
  final _userApi = UserApi();

  Future<void> createUser(User user) async {
    final authApi = AuthApi();

    final userId = authApi.getUserId();

    await _userApi.createUser(userId, user);
  }

  Future<User?> getUser() async {
    final authApi = AuthApi();

    final userId = authApi.getUserId();

    final user = await _userApi.getUser(userId);

    if (user != null && user.userType == 'admin') {
      return user;
    }

    return null;
  }

  Stream<DocumentSnapshot<User?>> getUserDataStream() {
    final authApi = AuthApi();

    final userId = authApi.getUserId();

    return _userApi.getUserStream(userId);
  }

  Future<void> updateUser(Map<String, dynamic> data) async {
    final authApi = AuthApi();

    final userId = authApi.getUserId();

    await _userApi.updateUser(userId, data);
  }

  Future<void> deleteUser() async {
    final authApi = AuthApi();

    final userId = authApi.getUserId();

    await _userApi.deleteUser(userId);
  }
}
