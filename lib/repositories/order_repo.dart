import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/apis/order_api.dart';
import 'package:provider_app/models/order.dart' as app_model;

class OrderRepository {
  Future<app_model.Order> getOrder(String orderId) async {
    final orderApi = OrderApi();

    final snapshot = await orderApi.getOrder(orderId);

    return snapshot.data()!;
  }

  Future<List<app_model.Order>> getOrders() async {
    final orderApi = OrderApi();

    final snapshot = await orderApi.getOrders();

    final orders = snapshot.docs.map((e) => e.data()).toList();

    return orders;
  }

  Stream<QuerySnapshot<app_model.Order>> getOrdersStream() {
    final orderApi = OrderApi();

    return orderApi.getOrdersStream();
  }

  Future<void> updateOrder(String orderId, Map<String, dynamic> data) async {
    final orderApi = OrderApi();

    await orderApi.updateOrder(orderId, data);
  }
}
