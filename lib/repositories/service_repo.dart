import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/apis/service_api.dart';
import 'package:provider_app/models/service.dart';

class ServiceRepository {
  final _serviceApi = ServiceApi();

  Future<void> createService(Map<String, dynamic> serviceData) async {
    await _serviceApi.createService(Service.fromJson(serviceData));
  }

  Future<List<Service>> getServices() async {
    final snapshot = await _serviceApi.getServices();

    return snapshot.docs.map((e) => e.data()).toList();
  }

  Stream<QuerySnapshot<Service>> getServicesStream() {
    return _serviceApi.getServicesStream();
  }

  Future<void> deleteService(String serviceId) async {
    await _serviceApi.deleteService(serviceId);
  }
}
