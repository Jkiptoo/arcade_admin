import 'package:provider_app/apis/auth_api.dart';

class AuthRepository {
  final _authApi = AuthApi();

  String? getUserId() => _authApi.getUserId();

  Future<void> signUp(String email, String password) async {
    await _authApi.signUpWithEmail(email, password);
  }

  Future<void> signIn({
    String? email,
    String? password,
    String? phoneNumber,
  }) async {
    if (email != null && password != null) {
      await _authApi.signInWithEmail(email, password);
    } else if (phoneNumber != null) {
      await _authApi.verifyPhone(phoneNumber);
    }
  }

  Future<void> linkPhoneToEmail(String phoneNumber) async {
    await _authApi.linkAuthProviders(phoneNumber: phoneNumber);
  }

  Future<void> unlinkPhoneFromEmail(String phoneNumber) async {
    await _authApi.unlinkAuthProviders(phoneNumber: phoneNumber);
  }

  Future<void> signout() async => await _authApi.signout();
}
