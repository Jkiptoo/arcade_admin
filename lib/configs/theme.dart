import 'dart:convert';

import 'package:flutter/material.dart';

const accent = Color(0xFF29EE86);

const error = Color(0xFFB00020);

const accentGradient = LinearGradient(colors: [Color(0xFF3FA0D7), accent]);

final appTheme = ThemeData(
  fontFamily: 'WorkSans',
  colorScheme: const ColorScheme.dark(
    primary: accent,
    secondary: accent,
  ),
  brightness: Brightness.dark,
  scaffoldBackgroundColor: const Color.fromRGBO(36, 34, 46, 1),
  drawerTheme: const DrawerThemeData(
    backgroundColor: Color.fromRGBO(36, 34, 46, 1),
  ),
  appBarTheme: const AppBarTheme(backgroundColor: Color(0xFF1F1E28)),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    showSelectedLabels: true,
    showUnselectedLabels: true,
    type: BottomNavigationBarType.fixed,
    backgroundColor: Color.fromRGBO(36, 34, 46, 1),
    selectedLabelStyle: TextStyle(fontSize: 12, color: accent),
    unselectedLabelStyle: TextStyle(fontSize: 10),
  ),
  cardTheme: const CardTheme(margin: EdgeInsets.zero),
  tabBarTheme: const TabBarTheme(
    labelPadding: EdgeInsets.all(4),
    labelColor: accent,
  ),
  dialogTheme: const DialogTheme(
    backgroundColor: Color.fromRGBO(36, 34, 46, 1),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.zero),
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelStyle: const TextStyle(
      color: Color.fromRGBO(52, 198, 173, 1),
    ),
    filled: true,
    fillColor: const Color.fromRGBO(74, 74, 98, 1),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(48),
      borderSide: const BorderSide(color: Colors.transparent),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(48),
      borderSide: const BorderSide(
        color: Color.fromRGBO(52, 198, 173, 1),
        width: 2,
      ),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(48),
      borderSide: const BorderSide(
        color: Color.fromARGB(255, 234, 62, 43),
        width: 2,
      ),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(48),
      borderSide: const BorderSide(
        color: Color.fromARGB(255, 234, 62, 43),
        width: 2,
      ),
    ),
    contentPadding: const EdgeInsets.symmetric(
      horizontal: 24,
      vertical: 16,
    ),
  ),
);

final mapStyle = jsonEncode([
  {
    "elementType": "geometry",
    "stylers": [
      {"color": "#212121"}
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#757575"}
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {"color": "#212121"}
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {"color": "#757575"}
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#9e9e9e"}
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#bdbdbd"}
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#757575"}
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {"color": "#181818"}
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#616161"}
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {"color": "#1b1b1b"}
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {"color": "#2c2c2c"}
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#8a8a8a"}
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {"color": "#373737"}
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {"color": "#3c3c3c"}
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {"color": "#4e4e4e"}
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#616161"}
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {"visibility": "off"}
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#757575"}
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {"color": "#000000"}
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {"color": "#3d3d3d"}
    ]
  }
]);
