import 'package:json_annotation/json_annotation.dart';

enum ImageType { profile, license, idFront, idBack }

enum UpdateStatus { started, completed }

enum AuthStatus { authenticated, unauthenticated }

enum LoadingStatus { initial, loading, success, failure }

enum PermissionType { location, storage, notifications }

enum TripStatus {
  @JsonValue("unknown")
  unknown,
  @JsonValue("began")
  began,
  @JsonValue("stopped")
  stopped,
}

enum OrderStatus {
  @JsonValue("requested")
  requested,
  @JsonValue("accepted")
  accepted,
  @JsonValue("picking")
  picking,
  @JsonValue("picked")
  picked,
  @JsonValue("processing")
  processing,
  @JsonValue("processed")
  processed,
  @JsonValue("delivering")
  delivering,
  @JsonValue("delivered")
  delivered,
  @JsonValue("paid")
  paid,
  @JsonValue("reviewed")
  reviewed,
  @JsonValue("cancelled")
  cancelled,
}

enum PaymentMethod {
  @JsonValue("cash")
  cash,
  @JsonValue("mpesa")
  mpesa,
}
