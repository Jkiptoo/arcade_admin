part of 'service_bloc.dart';

abstract class ServiceState extends Equatable {
  const ServiceState();

  @override
  List<Object?> get props => [];
}

class ServiceInitial extends ServiceState {}

class ServiceLoading extends ServiceState {}

class ServicesFetched extends ServiceState {
  final List<Service> services;
  final Stream<QuerySnapshot<Service>>? servicesStream;

  const ServicesFetched({this.services = const [], this.servicesStream});

  @override
  List<Object?> get props => [services, servicesStream];
}

class ServiceUpdated extends ServiceState {
  final String message;

  const ServiceUpdated({required this.message});

  @override
  List<Object> get props => [message];
}

class ServiceError extends ServiceState {
  final String message;

  const ServiceError({required this.message});

  @override
  List<Object> get props => [message];
}
