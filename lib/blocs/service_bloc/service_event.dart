part of 'service_bloc.dart';

abstract class ServiceEvent extends Equatable {
  const ServiceEvent();

  @override
  List<Object> get props => [];
}

class FetchServices extends ServiceEvent {}

class CreateService extends ServiceEvent {
  final Map<String, dynamic> data;

  const CreateService({required this.data});
}

class DeleteService extends ServiceEvent {
  final String serviceId;

  const DeleteService({required this.serviceId});
}
