import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/service.dart';
import 'package:provider_app/repositories/service_repo.dart';

part 'service_event.dart';
part 'service_state.dart';

class ServiceBloc extends Bloc<ServiceEvent, ServiceState> {
  ServiceBloc() : super(ServiceInitial()) {
    on<FetchServices>(_onFetchServices);
    on<CreateService>(_onCreateService);
    on<DeleteService>(_onDeleteService);
  }

  final _serviceRepo = ServiceRepository();

  void _onFetchServices(FetchServices event, Emitter<ServiceState> emit) async {
    emit(ServiceLoading());
    try {
      emit(ServicesFetched(
        services: await _serviceRepo.getServices(),
        servicesStream: _serviceRepo.getServicesStream(),
      ));
    } on CustomException catch (e) {
      emit(ServiceError(message: e.message));
    } on Exception catch (e) {
      log('$e');
    }
  }

  void _onCreateService(CreateService event, Emitter<ServiceState> emit) async {
    emit(ServiceLoading());
    try {
      await _serviceRepo.createService(event.data);
      emit(const ServiceUpdated(message: 'Service created'));
      emit(ServicesFetched(
        services: await _serviceRepo.getServices(),
        servicesStream: _serviceRepo.getServicesStream(),
      ));
    } on CustomException catch (e) {
      emit(ServiceError(message: e.message));
    } on Exception catch (e) {
      log('$e');
    }
  }

  void _onDeleteService(DeleteService event, Emitter<ServiceState> emit) async {
    emit(ServiceLoading());
    try {
      await _serviceRepo.deleteService(event.serviceId);
      emit(const ServiceUpdated(message: 'Service deleted'));
      emit(ServicesFetched(
        services: await _serviceRepo.getServices(),
        servicesStream: _serviceRepo.getServicesStream(),
      ));
    } on CustomException catch (e) {
      emit(ServiceError(message: e.message));
    } on Exception catch (e) {
      log('$e');
    }
  }
}
