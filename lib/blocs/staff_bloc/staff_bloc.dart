import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/models/staff.dart';
import 'package:provider_app/repositories/staff_repo.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'staff_event.dart';
part 'staff_state.dart';

class StaffBloc extends Bloc<StaffEvent, StaffState> {
  StaffBloc() : super(const StaffState()) {
    on<GetStaffDataStream>(_onGetStaffDataStream);
    on<CreateStaff>(_onCreateStaff);
    on<UpdateStaff>(_onUpdateStaff);
    on<DeleteStaff>(_onDeleteStaff);
  }

  final _staffRepo = StaffRepository();

  void _onGetStaffDataStream(
    GetStaffDataStream event,
    Emitter<StaffState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
        staffDataStream: _staffRepo.getStaff(),
      ));
    } on Exception catch (e) {
      log('$e');
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }

  void _onCreateStaff(
    CreateStaff event,
    Emitter<StaffState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      await _staffRepo.createStaff(event.staffData);

      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
      ));
    } on Exception catch (e) {
      log('$e');
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }

  void _onUpdateStaff(
    UpdateStaff event,
    Emitter<StaffState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      await _staffRepo.updateStaff(event.staffId, event.data);

      emit(state.copyWith(loadingStatus: LoadingStatus.success));
    } on Exception catch (e) {
      log('$e');
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }

  void _onDeleteStaff(
    DeleteStaff event,
    Emitter<StaffState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      await _staffRepo.deleteStaff(event.staffId);
      emit(state.copyWith(loadingStatus: LoadingStatus.success));
    } on Exception catch (e) {
      log('$e');
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }
}
