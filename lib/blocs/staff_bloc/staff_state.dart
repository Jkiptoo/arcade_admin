part of 'staff_bloc.dart';

class StaffState extends Equatable {
  final LoadingStatus loadingStatus;
  final Staff? staffData;
  final Stream<QuerySnapshot<Staff>>? staffDataStream;

  const StaffState({
    this.loadingStatus = LoadingStatus.initial,
    this.staffData,
    this.staffDataStream,
  });

  StaffState copyWith({
    LoadingStatus? loadingStatus,
    Staff? staffData,
    Stream<QuerySnapshot<Staff>>? staffDataStream,
  }) {
    return StaffState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      staffData: staffData ?? this.staffData,
      staffDataStream: staffDataStream ?? this.staffDataStream,
    );
  }

  @override
  List<Object?> get props => [
        loadingStatus,
        staffData,
        staffDataStream,
      ];
}
