part of 'staff_bloc.dart';

abstract class StaffEvent extends Equatable {
  const StaffEvent();

  @override
  List<Object> get props => [];
}

class GetStaffDataStream extends StaffEvent {}

class CreateStaff extends StaffEvent {
  final Map<String, dynamic> staffData;

  const CreateStaff({required this.staffData});
}

class UpdateStaff extends StaffEvent {
  final String staffId;
  final Map<String, dynamic> data;

  const UpdateStaff({required this.staffId, required this.data});
}

class DeleteStaff extends StaffEvent {
  final String staffId;

  const DeleteStaff({required this.staffId});
}
