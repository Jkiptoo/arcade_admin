part of 'auth_bloc.dart';

class AuthState extends Equatable {
  final AuthStatus authStatus;
  final LoadingStatus loadingStatus;
  final String? message;

  const AuthState({
    this.authStatus = AuthStatus.unauthenticated,
    this.loadingStatus = LoadingStatus.initial,
    this.message,
  });

  AuthState copyWith({
    AuthStatus? authStatus,
    LoadingStatus? loadingStatus,
    String? message,
  }) {
    return AuthState(
      authStatus: authStatus ?? this.authStatus,
      loadingStatus: loadingStatus ?? this.loadingStatus,
      message: message,
    );
  }

  @override
  List<Object?> get props => [authStatus, authStatus, message];
}
