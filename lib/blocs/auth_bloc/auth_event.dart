part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class SignUp extends AuthEvent {
  final String email, password;
  final String phoneNumber;

  const SignUp({
    required this.email,
    required this.password,
    required this.phoneNumber,
  });
}

class SignInWithEmail extends AuthEvent {
  final String email, password;

  const SignInWithEmail({required this.email, required this.password});
}

class SignInWithPhone extends AuthEvent {
  final String phoneNumber;

  const SignInWithPhone({required this.phoneNumber});
}

class SignOut extends AuthEvent {}
