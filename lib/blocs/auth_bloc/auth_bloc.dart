import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart' hide User;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/user.dart';
import 'package:provider_app/repositories/auth_repo.dart';
import 'package:provider_app/repositories/user_repo.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(const AuthState()) {
    on<SignUp>(_onSignUp);
    on<SignInWithEmail>(_onSignInWithEmail);
    on<SignInWithPhone>(_onSignInWithPhone);
    on<SignOut>(_onSignOut);
  }

  final _authRepo = AuthRepository();

  void _onSignUp(
    SignUp event,
    Emitter<AuthState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      final userRepo = UserRepository();
      await _authRepo.signUp(event.email, event.password);
      await userRepo.createUser(User(
        id: '',
        phoneNumber: event.phoneNumber,
        email: event.email,
      ));

      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
        authStatus: AuthStatus.authenticated,
      ));
    } on FirebaseException catch (e) {
      log('$e');
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: e.message,
      ));
    }
  }

  void _onSignInWithEmail(
    SignInWithEmail event,
    Emitter<AuthState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      final userRepo = UserRepository();
      await _authRepo.signIn(email: event.email, password: event.password);
      final user = await userRepo.getUser();
      if (user == null) {
        throw CustomException(
          code: ExceptionCode.invalidUser,
          message: 'Invalid credentials, user is not an admin',
        );
      }
      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
        authStatus: AuthStatus.authenticated,
      ));
    } on FirebaseException catch (e) {
      log('$e');
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: e.message,
      ));
    } on Exception catch (e) {
      log('$e');
      String? message = '$e';
      if (e is CustomException && e.code == ExceptionCode.invalidUser) {
        await _authRepo.signout();
        message = e.message;
      }
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: message,
      ));
    }
  }

  void _onSignInWithPhone(
    SignInWithPhone event,
    Emitter<AuthState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      final userRepo = UserRepository();
      await _authRepo.signIn(phoneNumber: event.phoneNumber);
      final user = await userRepo.getUser();
      if (user == null) {
        throw CustomException(
          code: ExceptionCode.invalidUser,
          message: 'Invalid credentials, user is not an admin',
        );
      }
      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
        authStatus: AuthStatus.authenticated,
      ));
    } on FirebaseException catch (e) {
      log('$e');
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: e.message,
      ));
    } on Exception catch (e) {
      log('$e');
      String? message = '$e';
      if (e is CustomException && e.code == ExceptionCode.invalidUser) {
        await _authRepo.signout();
        message = e.message;
      }
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: message,
      ));
    }
  }

  void _onSignOut(
    SignOut event,
    Emitter<AuthState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      await _authRepo.signout();
      emit(state.copyWith(
        loadingStatus: LoadingStatus.success,
        authStatus: AuthStatus.unauthenticated,
      ));
    } on FirebaseException catch (e) {
      log('$e');
      emit(state.copyWith(
        loadingStatus: LoadingStatus.failure,
        message: e.message,
      ));
    }
  }
}
