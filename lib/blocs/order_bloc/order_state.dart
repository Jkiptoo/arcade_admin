part of 'order_bloc.dart';

class OrderState extends Equatable {
  final LoadingStatus loadingStatus;
  final Map<String, dynamic>? orderData;

  const OrderState({
    this.loadingStatus = LoadingStatus.initial,
    this.orderData = const {},
  });

  OrderState copyWith({
    LoadingStatus? loadingStatus,
    Map<String, dynamic>? orderData,
  }) {
    return OrderState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      orderData: orderData ?? this.orderData,
    );
  }

  @override
  List<Object> get props => [
        loadingStatus,
        orderData!,
      ];
}

class OrdersFetched extends OrderState {
  final Stream<QuerySnapshot<app.Order>> ordersDataStream;

  const OrdersFetched({required this.ordersDataStream});

  @override
  List<Object> get props => [ordersDataStream];
}

class OrderSelected extends OrderState {
  final app.Order order;

  const OrderSelected({required this.order});

  @override
  List<Object> get props => [order];
}
