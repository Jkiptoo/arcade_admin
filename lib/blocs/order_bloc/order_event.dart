part of 'order_bloc.dart';

abstract class OrderEvent extends Equatable {
  const OrderEvent();

  @override
  List<Object> get props => [];
}

class GetOrders extends OrderEvent {
  final OrderStatus? status;

  const GetOrders({this.status});
}

class UpdateOrderData extends OrderEvent {
  final String orderId;
  final Map<String, dynamic> data;

  const UpdateOrderData({required this.orderId, required this.data});
}
