import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/models/order.dart' as app;
import 'package:provider_app/repositories/order_repo.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'order_event.dart';
part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  OrderBloc() : super(const OrderState()) {
    on<GetOrders>(_onGetOrders);
    on<UpdateOrderData>(_onUpdateOrderData);
  }

  void _onGetOrders(
    GetOrders event,
    Emitter<OrderState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      final orderRepo = OrderRepository();
      emit(OrdersFetched(ordersDataStream: orderRepo.getOrdersStream()));
    } catch (e) {
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }

  void _onUpdateOrderData(
    UpdateOrderData event,
    Emitter<OrderState> emit,
  ) async {
    emit(state.copyWith(loadingStatus: LoadingStatus.loading));
    try {
      final orderRepo = OrderRepository();
      await orderRepo.updateOrder(event.orderId, event.data);
      log('updated order data');
      emit(state.copyWith(loadingStatus: LoadingStatus.success));
    } catch (e) {
      log('$e');
      emit(state.copyWith(loadingStatus: LoadingStatus.failure));
    }
  }
}
