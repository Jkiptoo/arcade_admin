import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/repositories/auth_repo.dart';
import 'package:provider_app/services/notification_service.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc() : super(AppInitial()) {
    on<InitializeApp>(_onInitializeApp);
  }

  void _onInitializeApp(
    InitializeApp event,
    Emitter<AppState> emit,
  ) async {
    emit(AppLoading());
    try {
      final authRepo = AuthRepository();

      final userId = authRepo.getUserId();

      if (userId != null) {
        final notificationService = NotificationService();
        await notificationService.initialize();
      }

      emit(AppInitialized(currentUserId: userId));
    } on CustomException catch (e) {
      if (e.code == ExceptionCode.noUser) {
        emit(const AppInitialized());
      } else {
        emit(AppError(message: e.message));
      }
    } on Exception catch (e) {
      log('$e');
    }
  }
}
