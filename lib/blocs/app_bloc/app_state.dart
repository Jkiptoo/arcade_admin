part of 'app_bloc.dart';

abstract class AppState extends Equatable {
  const AppState();

  @override
  List<Object?> get props => [];
}

class AppInitial extends AppState {}

class AppLoading extends AppState {}

class AppInitialized extends AppState {
  final String? currentUserId;

  const AppInitialized({this.currentUserId});

  @override
  List<Object?> get props => [currentUserId];
}

class AppError extends AppState {
  final String message;

  const AppError({required this.message});

  @override
  List<Object> get props => [message];
}
