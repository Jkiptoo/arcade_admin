import 'package:provider_app/configs/enums.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:provider_app/models/driver.dart';
import 'package:provider_app/models/review.dart';
import 'package:provider_app/models/vehicle.dart';

part 'order.g.dart';

@JsonSerializable(explicitToJson: true)
class Order {
  final String id, customerId, customerName, dateCreated, orderStartDate;
  final Vehicle vehicleInfo;
  final OrderStatus orderStatus;
  final TripStatus tripStatus;
  final Review? review;
  final Driver? assignedDriver;
  final String? cancellationReason;

  const Order({
    required this.id,
    required this.dateCreated,
    required this.vehicleInfo,
    required this.customerId,
    required this.customerName,
    required this.orderStartDate,
    this.orderStatus = OrderStatus.requested,
    this.tripStatus = TripStatus.unknown,
    this.review,
    this.assignedDriver,
    this.cancellationReason,
  });

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  Map<String, dynamic> toJson() => _$OrderToJson(this);

}
