import 'package:json_annotation/json_annotation.dart';

part 'staff.g.dart';

@JsonSerializable(explicitToJson: true)
class Staff {
  final String id, firstName, lastName, userType;
  final String? phoneNumber, profilePictureUrl;
  final bool isAssigned, isOnline;
  final String? idFrontImageUrl, idBackImageUrl, licenseImageUrl;
  final String? email;

  const Staff({
    this.id = '',
    required this.firstName,
    required this.lastName,
    required this.userType,
    this.isAssigned = false,
    this.isOnline = false,
    this.email,
    this.phoneNumber,
    this.profilePictureUrl,
    this.idFrontImageUrl,
    this.idBackImageUrl,
    this.licenseImageUrl,
  });

  factory Staff.fromJson(Map<String, dynamic> json) => _$StaffFromJson(json);

  Map<String, dynamic> toJson() => _$StaffToJson(this);
}
