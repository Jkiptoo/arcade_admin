// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Staff _$StaffFromJson(Map<String, dynamic> json) => Staff(
      id: json['id'] as String? ?? '',
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      userType: json['userType'] as String,
      isAssigned: json['isAssigned'] as bool? ?? false,
      isOnline: json['isOnline'] as bool? ?? false,
      email: json['email'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      profilePictureUrl: json['profilePictureUrl'] as String?,
      idFrontImageUrl: json['idFrontImageUrl'] as String?,
      idBackImageUrl: json['idBackImageUrl'] as String?,
      licenseImageUrl: json['licenseImageUrl'] as String?,
    );

Map<String, dynamic> _$StaffToJson(Staff instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'userType': instance.userType,
      'phoneNumber': instance.phoneNumber,
      'profilePictureUrl': instance.profilePictureUrl,
      'isAssigned': instance.isAssigned,
      'isOnline': instance.isOnline,
      'idFrontImageUrl': instance.idFrontImageUrl,
      'idBackImageUrl': instance.idBackImageUrl,
      'licenseImageUrl': instance.licenseImageUrl,
      'email': instance.email,
    };
