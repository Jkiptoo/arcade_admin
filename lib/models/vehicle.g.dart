// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Vehicle _$VehicleFromJson(Map<String, dynamic> json) => Vehicle(
      brand: json['brand'] as String,
      model: json['model'] as String,
      regNumber: json['regNumber'] as String,
      pictureUrl: json['pictureUrl'] as String?,
    );

Map<String, dynamic> _$VehicleToJson(Vehicle instance) => <String, dynamic>{
      'brand': instance.brand,
      'model': instance.model,
      'regNumber': instance.regNumber,
      'pictureUrl': instance.pictureUrl,
    };
