import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'vehicle.g.dart';

@JsonSerializable(explicitToJson: true)
class Vehicle extends Equatable {
  final String brand, model, regNumber;
  final String? pictureUrl;

  const Vehicle({
    required this.brand,
    required this.model,
    required this.regNumber,
    this.pictureUrl,
  });

  factory Vehicle.fromJson(Map<String, dynamic> json) =>
      _$VehicleFromJson(json);

  Map<String, dynamic> toJson() => _$VehicleToJson(this);

  @override
  List<Object?> get props => [brand, model, regNumber, pictureUrl];
}
