import 'dart:developer';

enum ExceptionCode {
  unknown,
  noUser,
  invalidUser,
  smsFetchTimeout,
  serviceDisabled,
  permissionDenied,
  permissionPermanentlyDenied,
}

class CustomException implements Exception {
  late ExceptionCode code;
  late String message;

  CustomException({
    this.code = ExceptionCode.unknown,
    this.message = 'An unknown error occurred',
  }) {
    log('[Custom Exception]: $message');
  }
}
