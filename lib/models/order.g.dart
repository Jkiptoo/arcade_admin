// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) => Order(
      id: json['id'] as String,
      dateCreated: json['dateCreated'] as String,
      vehicleInfo:
          Vehicle.fromJson(json['vehicleInfo'] as Map<String, dynamic>),
      customerId: json['customerId'] as String,
      customerName: json['customerName'] as String,
      orderStartDate: json['orderStartDate'] as String,
      orderStatus:
          $enumDecodeNullable(_$OrderStatusEnumMap, json['orderStatus']) ??
              OrderStatus.requested,
      tripStatus:
          $enumDecodeNullable(_$TripStatusEnumMap, json['tripStatus']) ??
              TripStatus.unknown,
      review: json['review'] == null
          ? null
          : Review.fromJson(json['review'] as Map<String, dynamic>),
      assignedDriver: json['assignedDriver'] == null
          ? null
          : Driver.fromJson(json['assignedDriver'] as Map<String, dynamic>),
      cancellationReason: json['cancellationReason'] as String?,
    );

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'id': instance.id,
      'customerId': instance.customerId,
      'customerName': instance.customerName,
      'dateCreated': instance.dateCreated,
      'orderStartDate': instance.orderStartDate,
      'vehicleInfo': instance.vehicleInfo.toJson(),
      'orderStatus': _$OrderStatusEnumMap[instance.orderStatus]!,
      'tripStatus': _$TripStatusEnumMap[instance.tripStatus]!,
      'review': instance.review?.toJson(),
      'assignedDriver': instance.assignedDriver?.toJson(),
      'cancellationReason': instance.cancellationReason,
    };

const _$OrderStatusEnumMap = {
  OrderStatus.requested: 'requested',
  OrderStatus.accepted: 'accepted',
  OrderStatus.picking: 'picking',
  OrderStatus.picked: 'picked',
  OrderStatus.processing: 'processing',
  OrderStatus.processed: 'processed',
  OrderStatus.delivering: 'delivering',
  OrderStatus.delivered: 'delivered',
  OrderStatus.paid: 'paid',
  OrderStatus.reviewed: 'reviewed',
  OrderStatus.cancelled: 'cancelled',
};

const _$TripStatusEnumMap = {
  TripStatus.unknown: 'unknown',
  TripStatus.began: 'began',
  TripStatus.stopped: 'stopped',
};
