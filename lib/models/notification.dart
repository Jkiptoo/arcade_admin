class Notification {
  final String id;
  final String title;
  final String? body;
  final dynamic payload;

  Notification({
    required this.id,
    required this.title,
    required this.body,
    required this.payload,
  });
}
