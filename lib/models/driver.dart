import 'package:json_annotation/json_annotation.dart';

part 'driver.g.dart';

@JsonSerializable(explicitToJson: true)
class Driver {
  final String id, firstName, lastName;
  final String? profilePictureUrl;

  const Driver({
    this.id = '',
    required this.firstName,
    required this.lastName,
    this.profilePictureUrl,
  });

  factory Driver.fromJson(Map<String, dynamic> json) => _$DriverFromJson(json);

  Map<String, dynamic> toJson() => _$DriverToJson(this);
}
