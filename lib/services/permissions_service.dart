// import 'package:firebase_messaging/firebase_messaging.dart';
// ignore_for_file: constant_identifier_names

import 'package:provider_app/models/custom_exception.dart';
import 'package:location/location.dart';

import '../configs/enums.dart';

class PermissionsService {
  final _location = Location();
  // final _messaging = FirebaseMessaging.instance;

  Future<PermissionStatus> getLocationPermission() async {
    bool serviceEnabled;

    serviceEnabled = await _location.serviceEnabled();
    if (serviceEnabled == false) {
      serviceEnabled = await _location.requestService();
      if (serviceEnabled == false) {
        throw CustomException(
          code: ExceptionCode.serviceDisabled,
          message: 'Location service has not been enabled',
        );
      }
    }

    return await _location.hasPermission();
  }

  Future<void> requestPermission({required PermissionType type}) async {
    switch (type) {
      case PermissionType.location:
        await _location.requestPermission();
        break;
      case PermissionType.notifications:
        // await _messaging.requestPermission();
        break;
      default:
    }
  }
}
