import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class LocationService {
  Future<LatLng> getLocation() async {
    final location = await Location().getLocation();
    return LatLng(location.latitude!, location.longitude!);
  }
}
