import 'package:shared_preferences/shared_preferences.dart';

class CacheService {
  final _sharedPref = SharedPreferences.getInstance();

  Future<String?> getFCMToken() async {
    final pref = await _sharedPref;
    return pref.getString('FCMToken');
  }

  Future<void> setFCMToken(String token) async {
    final pref = await _sharedPref;
    await pref.setString('FCMToken', token);
  }
}
