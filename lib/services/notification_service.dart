import 'dart:developer';
import 'dart:io' show Platform;

import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/notification.dart';
import 'package:provider_app/repositories/user_repo.dart';
import 'package:provider_app/services/cache_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationService {
  final _notificationPlugin = FlutterLocalNotificationsPlugin();
  final _messaging = FirebaseMessaging.instance;

  Future<void> initialize() async {
    // TODO: find a way to use permissions service to request permissions with a pure function

    final fcmSettings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );

    if (fcmSettings.authorizationStatus == AuthorizationStatus.denied) {
      throw CustomException(
        code: ExceptionCode.permissionDenied,
        message: 'Please allow notifications',
      );
    }

    const androidSettings = AndroidInitializationSettings('app_icon');
    const iosSettings = IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );

    const pluginSettings = InitializationSettings(
      android: androidSettings,
      iOS: iosSettings,
    );

    await _notificationPlugin.initialize(pluginSettings);

    if (Platform.isIOS) {
      final result = await _notificationPlugin
              .resolvePlatformSpecificImplementation<
                  IOSFlutterLocalNotificationsPlugin>()
              ?.requestPermissions(alert: true, badge: true, sound: true) ??
          false;

      if (result == false) {
        throw CustomException(
          code: ExceptionCode.permissionDenied,
          message: 'Notifications permission denied',
        );
      }
    }

    final cacheService = CacheService();
    final token = await cacheService.getFCMToken();
    log(token ?? 'null, getting new token');
    if (token == null) {
      final newToken = await _messaging.getToken();
      if (newToken != null) {
        log(newToken);
        await cacheService.setFCMToken(newToken);
        final userRepo = UserRepository();
        await userRepo.updateUser({'fcmToken': newToken});
      }
    }

    _messaging.onTokenRefresh.listen((token) async {
      final userRepo = UserRepository();
      await userRepo.updateUser({'fcmToken': token});
    });

    FirebaseMessaging.onMessage.listen((message) {
      log('Got a message whilst in the foreground!');
      log('Message data: ${message.data}');

      if (message.notification != null) {
        showNotification(
          Notification(
            id: message.messageId!,
            title: message.notification!.title!,
            body: message.notification!.body!,
            payload: message.data,
          ),
        );
      }
    });

    FirebaseMessaging.onBackgroundMessage(fcmBackgroundHandler);
  }

  Future<void> showNotification(Notification notification) async {
    const androidDetails = AndroidNotificationDetails(
      'user',
      'customer',
      importance: Importance.max,
      priority: Priority.defaultPriority,
    );

    const notificationDetails = NotificationDetails(android: androidDetails);

    await _notificationPlugin.show(
      0,
      notification.title,
      notification.body,
      notificationDetails,
    );
  }
}

Future<void> fcmBackgroundHandler(RemoteMessage message) async {
  log("Handling a background message: ${message.messageId}");
  if (message.notification != null) {
    final notificationService = NotificationService();
    await notificationService.showNotification(
      Notification(
        id: message.messageId!,
        title: message.notification!.title!,
        body: message.notification!.body!,
        payload: message.data,
      ),
    );
  }
}
