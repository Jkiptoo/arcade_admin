import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider_app/blocs/app_bloc/app_bloc.dart';
import 'package:provider_app/blocs/auth_bloc/auth_bloc.dart';
import 'package:provider_app/firebase_options.dart';
import 'package:geoflutterfire2/geoflutterfire2.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

Future<void> setup() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  locator.registerLazySingleton<GeoFlutterFire>(() => GeoFlutterFire());
  locator.registerLazySingleton<AppBloc>(() => AppBloc());
  locator.registerLazySingleton<AuthBloc>(() => AuthBloc());
}
