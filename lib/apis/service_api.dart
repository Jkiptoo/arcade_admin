import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/service.dart';

class ServiceApi {
  final _ref =
      FirebaseFirestore.instance.collection('services').withConverter<Service>(
            fromFirestore: (snapshot, _) {
              final data = snapshot.data();
              if (data == null) throw CustomException(message: 'No data');
              return Service.fromJson({...snapshot.data()!, 'id': snapshot.id});
            },
            toFirestore: (model, _) => model.toJson(),
          );

  Future<QuerySnapshot<Service>> getServices() async {
    return await _ref.get();
  }

  Stream<QuerySnapshot<Service>> getServicesStream() {
    return _ref.snapshots();
  }

  Future<void> createService(Service serviceData) async {
    final docRef = await _ref.add(serviceData);
    await _ref.doc(docRef.id).update({'id': docRef.id});
  }

  Future<void> editService(String serviceId, Map<String, dynamic> data) async {
    await _ref.doc(serviceId).update(data);
  }

  Future<void> deleteService(String serviceId) async {
    await _ref.doc(serviceId).delete();
  }
}
