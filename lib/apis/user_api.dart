import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/user.dart';

class UserApi {
  final _ref =
      FirebaseFirestore.instance.collection('users').withConverter<User>(
            fromFirestore: (snapshot, _) {
              final data = snapshot.data();
              if (data == null) throw CustomException(message: 'No data');
              return User.fromJson(data);
            },
            toFirestore: (user, _) => user.toJson(),
          );

  Future<void> createUser(String userId, User userData) async {
    await _ref.doc(userId).set(userData);
    await _ref.doc(userId).update({'id': userId});
  }

  Future<User?> getUser(String userId) async {
    final snapshot = await _ref.doc(userId).get();

    return snapshot.data();
  }

  Stream<DocumentSnapshot<User>> getUserStream(String userId) {
    return _ref.doc(userId).snapshots();
  }

  Future<void> updateUser(String userId, Map<String, dynamic> data) async {
    await _ref.doc(userId).update(data);
  }

  Future<void> deleteUser(String userId) async {
    await _ref.doc(userId).delete();
  }
}
