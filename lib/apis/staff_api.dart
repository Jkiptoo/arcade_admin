import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/staff.dart';

class StaffApi {
  final _ref =
      FirebaseFirestore.instance.collection('users').withConverter<Staff>(
            fromFirestore: (snapshot, _) {
              final data = snapshot.data();
              if (data == null) throw CustomException(message: 'No data');
              return Staff.fromJson({...snapshot.data()!, 'id': snapshot.id});
            },
            toFirestore: (model, _) => model.toJson(),
          );

  Future<Staff?> getStaffUser(String staffId) async {
    final snapshot = await _ref.doc(staffId).get();

    return snapshot.data();
  }

  Stream<QuerySnapshot<Staff>> getStaffStream() {
    return _ref.where('userType', whereIn: ['driver', 'cleaner']).snapshots();
  }

  Future<void> createStaffMember(Staff staffData) async {
    final docRef = await _ref.add(staffData);

    await _ref.doc(docRef.id).update({'id': docRef.id});
  }

  Future<void> updateStaffMember(
    String staffId,
    Map<String, dynamic> data,
  ) async {
    await _ref.doc(staffId).update(data);
  }

  Future<void> deleteStaffMember(String staffId) async {
    await _ref.doc(staffId).delete();
  }
}
