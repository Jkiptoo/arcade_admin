import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider_app/models/custom_exception.dart';

class AuthApi {
  final _auth = FirebaseAuth.instance;

  String getUserId() {
    final user = _auth.currentUser;

    if (user == null) {
      throw CustomException(
        code: ExceptionCode.noUser,
        message: 'No logged in user found',
      );
    }

    return user.uid;
  }

  Future<String?> verifyPhone(String phoneNumber) async {
    String? verificationId;

    await _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (phoneAuthCredential) async {
        await _auth.signInWithCredential(phoneAuthCredential);
      },
      verificationFailed: (error) => throw error,
      codeSent: (verificationId, forceResendingToken) {
        verificationId = verificationId;
      },
      codeAutoRetrievalTimeout: (verificationId) {
        throw CustomException(
          code: ExceptionCode.smsFetchTimeout,
          message: 'Please enter the code sent via SMS',
        );
      },
    );

    return verificationId;
  }

  Future<void> signUpWithEmail(String email, String password) async {
    final userCredential = await _auth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    if (userCredential.credential != null) {
      await _auth.signInWithCredential(userCredential.credential!);
    }
  }

  Future<void> signInWithEmail(String email, String password) async {
    await _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<void> linkAuthProviders({String? email, String? phoneNumber}) async {
    final providers = _auth.currentUser?.providerData ?? [];
    for (var provider in providers) {
      log('$provider');
    }
  }

  Future<void> unlinkAuthProviders({String? email, String? phoneNumber}) async {
    final providers = _auth.currentUser?.providerData ?? [];
    for (var provider in providers) {
      log('$provider');
    }
  }

  Future<void> deleteUser() async => _auth.currentUser?.delete();

  Future<void> signout() async => await _auth.signOut();
}
