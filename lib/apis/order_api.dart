import 'package:cloud_firestore/cloud_firestore.dart' hide Order;
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/models/custom_exception.dart';
import 'package:provider_app/models/order.dart' as app;
import 'package:provider_app/models/order.dart';

class OrderApi {
  final _ref =
      FirebaseFirestore.instance.collection('orders').withConverter<app.Order>(
            fromFirestore: (snapshot, _) {
              final data = snapshot.data();
              if (data == null) throw CustomException(message: 'No data');
              return Order.fromJson({...snapshot.data()!, 'id': snapshot.id});
            },
            toFirestore: (model, _) => model.toJson(),
          );

  Future<DocumentSnapshot<app.Order>> getOrder(String orderId) async {
    final query = _ref.doc(orderId);
    return await query.get();
  }

  Future<QuerySnapshot<app.Order>> getOrders() async {
    return await _ref.get();
  }

  Stream<QuerySnapshot<app.Order>> getOrdersStream({OrderStatus? status}) {
    if (status != null) {
      final query = _ref.where('orderStatus', isEqualTo: status.name);
      return query.snapshots();
    }
    return _ref.snapshots();
  }

  Future<void> updateOrder(String orderId, Map<String, dynamic> data) async {
    await _ref.doc(orderId).update(data);
  }
}
