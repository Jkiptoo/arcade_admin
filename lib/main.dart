import 'package:provider_app/configs/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider_app/locator.dart';
import 'package:provider_app/router.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider_app/blocs/app_bloc/app_bloc.dart';
import 'package:provider_app/blocs/auth_bloc/auth_bloc.dart';
import 'package:geoflutterfire2/geoflutterfire2.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  locator.registerLazySingleton<GeoFlutterFire>(() => GeoFlutterFire());
  locator.registerLazySingleton<AppBloc>(() => AppBloc());
  locator.registerLazySingleton<AuthBloc>(() => AuthBloc());
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Barcade Carwash - Provider',
      routerDelegate: router.routerDelegate,
      routeInformationParser: router.routeInformationParser,
      routeInformationProvider: router.routeInformationProvider,
      themeMode: ThemeMode.dark,
      darkTheme: appTheme,
    );
  }
}
