extension StringExtension on String {
  String capitalize() {
    if (length < 2) return this;

    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}
