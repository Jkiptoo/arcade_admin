import 'package:provider_app/blocs/auth_bloc/auth_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/screens/widgets/custom_input.dart';
import 'package:provider_app/screens/widgets/gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({super.key});

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  late final AuthBloc _authBloc;
  final _formKey = GlobalKey<FormState>();

  late final List<TextEditingController> _controllers;

  @override
  void initState() {
    super.initState();
    _authBloc = AuthBloc();
    _controllers = List.generate(3, (index) => TextEditingController());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      bloc: _authBloc,
      listener: (context, state) {
        if (state.loadingStatus == LoadingStatus.success &&
            state.authStatus == AuthStatus.authenticated) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Signed in'),
          ));
          context.goNamed('Home');
        }

        if (state.loadingStatus == LoadingStatus.failure) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              state.message ?? 'Unknown error occured. please try again',
            ),
          ));
        }
      },
      builder: (context, state) {
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(24),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  const Spacer(),
                  Text(
                    'SIGN IN TO YOUR ADMIN ACCOUNT',
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const Divider(indent: 24, endIndent: 24),
                  const SizedBox(height: 16),
                  CustomInput(
                    controller: _controllers[0],
                    labelText: 'Email',
                    textInputType: TextInputType.emailAddress,
                  ),
                  // const SizedBox(height: 16),
                  // CustomInput(
                  //   controller: _controllers[1],
                  //   labelText: 'Phone Number',
                  //   textInputType: TextInputType.phone,
                  //   validator: (value) {
                  //     if (value?.isEmpty ?? false) {
                  //       return 'Please enter a phone number';
                  //     }
                  //     return null;
                  //   },
                  // ),
                  const SizedBox(height: 16),
                  CustomInput(
                    controller: _controllers[2],
                    labelText: 'Password',
                    textInputType: TextInputType.visiblePassword,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {},
                        style:
                            TextButton.styleFrom(shape: const StadiumBorder()),
                        child: const Text('Reset Password?'),
                      )
                    ],
                  ),
                  const SizedBox(height: 24),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 2,
                        child: GradientButton(
                          title: 'signup',
                          isOutlined: true,
                          onTap: () => context.goNamed('SignUp'),
                        ),
                      ),
                      const SizedBox(width: 24),
                      Expanded(
                        flex: 3,
                        child: GradientButton(
                          title: 'login',
                          isLoading:
                              state.loadingStatus == LoadingStatus.loading,
                          onTap: () {
                            if (_formKey.currentState?.validate() ?? false) {
                              _authBloc.add(SignInWithEmail(
                                email: _controllers[0].text,
                                password: _controllers[2].text,
                              ));
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
