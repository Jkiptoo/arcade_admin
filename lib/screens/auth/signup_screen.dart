import 'package:provider_app/blocs/auth_bloc/auth_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/screens/widgets/custom_input.dart';
import 'package:provider_app/screens/widgets/gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  late final AuthBloc _authBloc;

  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _authBloc = AuthBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      bloc: _authBloc,
      listener: (context, state) {
        if (state.authStatus == AuthStatus.authenticated) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Created account, signing you in...'),
          ));
          context.goNamed('Home');
        }

        if (state.loadingStatus == LoadingStatus.failure) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              state.message ?? 'Unknown error occured. please try again',
            ),
          ));
        }
      },
      builder: (context, state) {
        return SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(24),
            reverse: true,
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Text(
                    'SIGNUP',
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const Divider(indent: 24, endIndent: 24),
                  const SizedBox(height: 16),
                  CustomInput(
                    controller: _emailController,
                    labelText: 'Email',
                    textInputType: TextInputType.emailAddress,
                  ),
                  const SizedBox(height: 16),
                  CustomInput(
                    controller: _phoneController,
                    labelText: 'Phone Number',
                    textInputType: TextInputType.phone,
                  ),
                  const SizedBox(height: 16),
                  CustomInput(
                    controller: _passwordController,
                    labelText: 'Password',
                    textInputType: TextInputType.visiblePassword,
                  ),
                  const SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 2,
                        child: GradientButton(
                          title: 'login',
                          isOutlined: true,
                          onTap: () => context.goNamed('SignIn'),
                        ),
                      ),
                      const SizedBox(width: 24),
                      Expanded(
                        flex: 3,
                        child: GradientButton(
                          title: 'sign up',
                          isLoading:
                              state.loadingStatus == LoadingStatus.loading,
                          onTap: () {
                            if (_formKey.currentState?.validate() ?? false) {
                              _authBloc.add(SignUp(
                                email: _emailController.text,
                                password: _passwordController.text,
                                phoneNumber: _phoneController.text,
                              ));
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
