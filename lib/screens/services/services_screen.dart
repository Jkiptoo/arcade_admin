import 'package:provider_app/blocs/service_bloc/service_bloc.dart';
import 'package:provider_app/screens/services/add_service_tile.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ServicesScreen extends StatefulWidget {
  const ServicesScreen({super.key});

  @override
  State<ServicesScreen> createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  late final ServiceBloc _serviceBloc;

  @override
  void initState() {
    super.initState();
    _serviceBloc = ServiceBloc()..add(FetchServices());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ServiceBloc, ServiceState>(
      bloc: _serviceBloc,
      listener: (context, state) {
        if (state is ServiceUpdated) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(state.message)),
          );
        }
      },
      builder: (context, state) {
        return Column(
          children: [
            const CustomAppBar(title: 'Add/Remove Services'),
            const AddServiceTile(),
            if (state is ServicesFetched)
              Expanded(
                child: StreamBuilder(
                    stream: state.servicesStream,
                    builder: (context, snapshot) {
                      if (snapshot.data == null) return const SizedBox();
                      return ListView.builder(
                        itemBuilder: (context, index) {
                          final service = snapshot.data!.docs[index].data();
                          return Container(

                              margin: const EdgeInsets.all(10.0),
                          padding: const EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                          Radius.circular(20.0) //                 <--- border radius here
                          ),
                          border: Border.all(color: Colors.greenAccent,   width: 0.5)
                          ),
                          child:ListTile(
                            title: Text(service.name),
                            subtitle: Text(
                              'Ksh.${service.cost.toStringAsFixed(0)}',
                            ),
                            trailing: IconButton(
                              onPressed: () {
                                _serviceBloc.add(DeleteService(
                                  serviceId: service.id,
                                ));
                              },
                              icon: const Icon(Icons.delete_rounded),
                            ),
                          ));
                        },
                        itemCount: snapshot.data!.docs.length,
                      );
                    }),
              ),
          ],
        );
      },
    );
  }
}
