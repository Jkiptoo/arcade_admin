import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/blocs/service_bloc/service_bloc.dart';
import 'package:provider_app/configs/theme.dart';
import 'package:provider_app/screens/widgets/custom_input.dart';
import 'package:provider_app/screens/widgets/gradient_button.dart';

class AddServiceTile extends StatefulWidget {
  const AddServiceTile({super.key});

  @override
  State<AddServiceTile> createState() => _AddServiceTileState();
}

class _AddServiceTileState extends State<AddServiceTile> {
  late final ServiceBloc _serviceBloc;

  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _costController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _serviceBloc = ServiceBloc();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      leading: const CircleAvatar(
        radius: 24,
        backgroundColor: accent,
        backgroundImage: AssetImage('assets/images/gradient.png'),
        child: Icon(Icons.garage_rounded, color: Colors.white),
      ),
      title: const Text('Add A Service'),
      trailing: const Icon(Icons.add_rounded),
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return BlocConsumer<ServiceBloc, ServiceState>(
              bloc: _serviceBloc,
              listener: (context, state) {
                if (state is ServiceUpdated) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(state.message),
                  ));
                  Navigator.of(context).pop();
                }
              },
              builder: (context, state) {
                return Form(
                  key: _formKey,
                  child: SimpleDialog(
                    title: const Center(child: Text('Add Service Info')),
                    contentPadding: const EdgeInsets.all(24),
                    children: [
                      CustomInput(
                        labelText: 'Service Name',
                        controller: _nameController,
                      ),
                      const SizedBox(height: 16),
                      CustomInput(
                        labelText: 'Cost(Kshs)',
                        controller: _costController,
                        textInputType: TextInputType.number,
                      ),
                      const SizedBox(height: 16),
                      GradientButton(
                        title: 'add',
                        onTap: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            _serviceBloc.add(
                              CreateService(data: {
                                'cost': int.parse(_costController.text),
                                'name': _nameController.text.trim(),
                              }),
                            );
                          }
                        },
                      )
                    ],
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
