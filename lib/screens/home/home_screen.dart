import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';
import 'package:provider_app/blocs/order_bloc/order_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final OrderBloc _orderBloc;

  @override
  void initState() {
    super.initState();
    _orderBloc = OrderBloc()..add(const GetOrders());
  }


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      bloc: _orderBloc,
      builder: (context, state) {
        if (state is OrdersFetched) {
          return Column(
            children: [
              const CustomAppBar(
                  title: 'All Orders'),
              Expanded(
                  child: StreamBuilder(
                    stream: state.ordersDataStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                        return ListView.builder(
                          itemCount: 15,
                          itemBuilder: (context, index) {
                            final order = snapshot.data!.docs[index].data();
                            final orderStartDate = order.orderStartDate;

                            // ignore: unnecessary_null_comparison

                            return GestureDetector(
                              onTap: () {
                                context.goNamed('AssignOrder', extra: order);
                              },
                              child: Container(
                                margin: const EdgeInsets.all(10.0),
                                padding: const EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20.0),
                                  ),
                                  border: Border.all(color: Colors.greenAccent, width: 0.5),
                                ),
                                child: ListTile(
                                  isThreeLine: false,
                                  enabled: order.orderStatus == OrderStatus.requested ||
                                      order.orderStatus == OrderStatus.picked ||
                                      order.orderStatus == OrderStatus.processing,
                                  title: Text(
                                    'Order Date: ${orderStartDate != null ? DateFormat().format(DateTime.parse(orderStartDate)) : ''}',
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(height: 4),
                                      Text('Customer Name: ${order.customerName}'),
                                      const SizedBox(height: 4),
                                      Text(
                                        'Order Status: ${order.orderStatus.name.toUpperCase()}',
                                      ),
                                      const SizedBox(height: 4),
                                      if (order.assignedDriver != null)
                                        Text(
                                          'Driver Name: ${order.assignedDriver?.firstName} ${order.assignedDriver?.lastName}',
                                        ),
                                      if (order.orderStatus == OrderStatus.cancelled)
                                        const SizedBox(height: 4),
                                      if (order.orderStatus == OrderStatus.cancelled)
                                        Text(
                                          'Cancellation Reason: ${order.cancellationReason}',
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      } else if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicator();
                      } else {
                        return Text('No data available');
                      }
                    },
                  )




              ),
            ],
          );
        }
        return const SizedBox();
      },
    );
  }
}


// Future<void> getPosts() async {
//   final QuerySnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore.instance
//       .collection('posts')
//       .orderBy('date', descending: true)
//       .get();
//
//   snapshot.docs.forEach((doc) {
//     print(doc.data());
//   });
// }