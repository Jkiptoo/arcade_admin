import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/blocs/order_bloc/order_bloc.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';

class PastOrdersScreen extends StatefulWidget {
  const PastOrdersScreen({super.key});

  @override
  State<PastOrdersScreen> createState() => _PastOrdersScreenState();
}

class _PastOrdersScreenState extends State<PastOrdersScreen> {
  late final OrderBloc _orderBloc;

  @override
  void initState() {
    super.initState();
    _orderBloc = context.read<OrderBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      bloc: _orderBloc,
      builder: (context, state) {
        return Column(
          children: [
            const CustomAppBar(title: 'Past Orders'),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return const ListTile(
                    title: Text('order'),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
