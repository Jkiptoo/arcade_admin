import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:provider_app/blocs/order_bloc/order_bloc.dart';
import 'package:provider_app/blocs/staff_bloc/staff_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/models/order.dart';
import 'package:provider_app/models/staff.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';

class AssignOrderScreen extends StatefulWidget {
  final Order order;

  const AssignOrderScreen({super.key, required this.order});

  @override
  State<AssignOrderScreen> createState() => _AssignOrderScreenState();
}

class _AssignOrderScreenState extends State<AssignOrderScreen> {
  late final StaffBloc _staffBloc;
  late final OrderBloc _orderBloc;

  @override
  void initState() {
    super.initState();
    _staffBloc = StaffBloc()..add(GetStaffDataStream());
    _orderBloc = OrderBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StaffBloc, StaffState>(
      bloc: _staffBloc,
      builder: (context, state) {
        return Column(
          children: [
            const CustomAppBar(title: 'Assign The Order'),
            Expanded(
              child: StreamBuilder(
                stream: state.staffDataStream,
                builder: (context, snapshot) {
                  if (snapshot.data == null) return const SizedBox();
                  return ListView.builder(
                    itemBuilder: (context, index) {
                      final staff = snapshot.data!.docs[index].data();
                      return ListTile(
                        title:
                            Text('Name: ${staff.firstName} ${staff.lastName}'),
                        subtitle: Text('Type: ${staff.userType}'),
                        enabled: _isEnabled(staff, widget.order.orderStatus),
                        onTap: () {
                          if (staff.isOnline && !staff.isAssigned) {
                            if (staff.userType == 'driver') {
                              if (widget.order.orderStatus ==
                                  OrderStatus.processing) {
                                _orderBloc.add(UpdateOrderData(
                                  orderId: widget.order.id!,
                                  data: {
                                    'assignedDriver': staff.toJson(),
                                    'orderStatus': OrderStatus.processed.name,
                                  },
                                ));
                              } else {
                                _orderBloc.add(UpdateOrderData(
                                  orderId: widget.order.id!,
                                  data: {
                                    'assignedDriver': staff.toJson(),
                                    'orderStatus': OrderStatus.accepted.name,
                                    'otp': Random().nextInt(8999) + 1000
                                  },
                                ));
                              }
                            } else {
                              _orderBloc.add(UpdateOrderData(
                                orderId: widget.order.id!,
                                data: {
                                  'assignedCleaner': staff.toJson(),
                                  'orderStatus': OrderStatus.processing.name,
                                },
                              ));
                            }
                            context.pop();
                          }
                        },
                      );
                    },
                    itemCount: snapshot.data!.docs.length,
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }

  bool _isEnabled(Staff staff, OrderStatus status) {
    if (staff.userType == 'driver') {
      return staff.isOnline &&
          !staff.isAssigned &&
          (status == OrderStatus.requested || status == OrderStatus.processing);
    }
    if (staff.userType == 'cleaner') {
      return staff.isAssigned == false && (status == OrderStatus.picked);
    }
    return false;
  }
}
