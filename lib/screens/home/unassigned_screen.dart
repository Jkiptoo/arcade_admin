import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/blocs/order_bloc/order_bloc.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';

class UnassignedOrdersScreen extends StatefulWidget {
  const UnassignedOrdersScreen({super.key});

  @override
  State<UnassignedOrdersScreen> createState() => _UnassignedOrdersScreenState();
}

class _UnassignedOrdersScreenState extends State<UnassignedOrdersScreen> {
  late final OrderBloc _orderBloc;

  @override
  void initState() {
    super.initState();
    _orderBloc = context.read<OrderBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      bloc: _orderBloc,
      builder: (context, state) {
        return Column(
          children: [
            const CustomAppBar(title: 'Unassigned Orders'),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return const ListTile(
                    title: Text('order'),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
