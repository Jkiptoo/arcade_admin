import 'package:provider_app/blocs/app_bloc/app_bloc.dart';
import 'package:provider_app/locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late final AppBloc _appBloc;

  @override
  void initState() {
    super.initState();
    _appBloc = locator.get<AppBloc>()..add(InitializeApp());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      bloc: _appBloc,
      builder: (context, state) => FutureBuilder(
        future: Future.delayed(const Duration(seconds: 3), () {
          if (state is AppInitialized) {
            if (state.currentUserId == null) {
              context.goNamed('SignIn');
            } else {
              context.goNamed('Home');
            }
          }
        }),
        builder: (context, snapshot) {
          return Container(
            decoration: const BoxDecoration(
              color: Color(0xFF121041),
              image: DecorationImage(
                image: AssetImage('assets/images/splash.png'),
                fit: BoxFit.contain,
              ),
            ),
          );
        },
      ),
    );
  }
}
