import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:provider_app/blocs/staff_bloc/staff_bloc.dart';
import 'package:provider_app/configs/theme.dart';
import 'package:provider_app/screens/staff/add_staff_tile.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';

class StaffScreen extends StatefulWidget {
  const StaffScreen({super.key});

  @override
  State<StaffScreen> createState() => _StaffScreenState();
}

class _StaffScreenState extends State<StaffScreen> {
  late final StaffBloc _staffBloc;

  @override
  void initState() {
    super.initState();
    _staffBloc = StaffBloc()..add(GetStaffDataStream());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StaffBloc, StaffState>(
      bloc: _staffBloc,
      builder: (context, state) {
        return StreamBuilder(
            stream: state.staffDataStream,
            builder: (context, snapshot) {
              if (snapshot.data == null) return const SizedBox();
              final staff = snapshot.data!.docs;
              return Column(
                children: [
                  const CustomAppBar(title: 'Monitor Your Staff'),
                  const AddStaffTile(),
                  Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, index) {
                        final staffMember = staff[index].data();
                        return Container(

                            margin: const EdgeInsets.all(10.0),
                        padding: const EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                        Radius.circular(20.0) //                 <--- border radius here
                        ),
                        border: Border.all(color: Colors.greenAccent,   width: 0.5)
                        ),
                        child:ListTile(
                          isThreeLine: true,
                          title: Text(
                            '${staffMember.firstName} ${staffMember.lastName}',
                          ),
                          subtitle: Row( crossAxisAlignment:  CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.spaceBetween,children:[
                            Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 4),
                              Text(
                                'Type: ${staffMember.userType}'.toUpperCase(),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                (staffMember.isAssigned
                                        ? 'assigned'
                                        : 'not assigned')
                                    .toUpperCase(),
                              ),
                              const SizedBox(height: 4),

                              ]),
                            if (staffMember.userType == 'driver')
                              Row(
                                children: [
                                  SizedBox.square(
                                    dimension: 8,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: staffMember.isOnline
                                            ? Colors.green
                                            : error,
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  Text(
                                    (staffMember.isOnline
                                        ? 'online'
                                        : 'offline')
                                        .toUpperCase(),
                                    style: TextStyle(
                                      color: staffMember.isOnline
                                          ? Colors.green
                                          : error,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                          onTap: () {
                            context.goNamed('StaffProfile', extra: staffMember);
                          },
                        ));
                      },
                      itemCount: staff.length,
                    ),
                  ),
                ],
              );
            });
      },
    );
  }
}
