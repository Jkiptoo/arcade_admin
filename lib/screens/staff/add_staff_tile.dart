import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider_app/blocs/staff_bloc/staff_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/configs/theme.dart';
import 'package:provider_app/screens/widgets/custom_input.dart';
import 'package:provider_app/screens/widgets/gradient_button.dart';

class AddStaffTile extends StatefulWidget {
  const AddStaffTile({super.key});

  @override
  State<AddStaffTile> createState() => _AddStaffTileState();
}

class _AddStaffTileState extends State<AddStaffTile> {
  late final StaffBloc _staffBloc;

  late final List<TextEditingController> _controllers;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _staffBloc = StaffBloc();
    _controllers = List.generate(
      3,
      (index) => TextEditingController(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      leading: const CircleAvatar(
        radius: 24,
        backgroundColor: accent,
        backgroundImage: AssetImage('assets/images/gradient.png'),
        child: Icon(Icons.group_rounded, color: Colors.white),
      ),
      title: const Text('Add A Cleaner'),
      trailing: const Icon(Icons.add_rounded),
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return BlocConsumer<StaffBloc, StaffState>(
              bloc: _staffBloc,
              listener: (context, state) {
                if (state.loadingStatus == LoadingStatus.success) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Succesfully added staff'),
                  ));
                  Navigator.of(context).pop();
                }
              },
              builder: (context, state) {
                return Form(
                  key: _formKey,
                  child: SimpleDialog(
                    title: const Center(child: Text('Add Staff Info')),
                    contentPadding: const EdgeInsets.all(24),
                    children: [
                      CustomInput(
                        labelText: 'First Name',
                        controller: _controllers[0],
                      ),
                      const SizedBox(height: 16),
                      CustomInput(
                        labelText: 'Last Name',
                        controller: _controllers[1],
                      ),
                      const SizedBox(height: 16),
                      GradientButton(
                        title: 'add',
                        onTap: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            _staffBloc.add(
                              CreateStaff(staffData: {
                                'firstName': _controllers[0].text.trim(),
                                'lastName': _controllers[1].text.trim(),
                                'userType': 'cleaner',
                                'isOnline': true,
                              }),
                            );
                          }
                        },
                      )
                    ],
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
