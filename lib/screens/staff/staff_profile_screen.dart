import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:provider_app/blocs/staff_bloc/staff_bloc.dart';
import 'package:provider_app/configs/enums.dart';
import 'package:provider_app/configs/theme.dart';
import 'package:provider_app/models/staff.dart';
import 'package:provider_app/screens/widgets/custom_appbar.dart';
import 'package:text_scroll/text_scroll.dart';

class StaffProfileScreen extends StatefulWidget {
  final Staff staff;

  const StaffProfileScreen({super.key, required this.staff});

  @override
  State<StaffProfileScreen> createState() => _StaffProfileScreenState();
}

class _StaffProfileScreenState extends State<StaffProfileScreen> {
  late final StaffBloc _staffBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _staffBloc = StaffBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<StaffBloc, StaffState>(
      bloc: _staffBloc,
      listener: (context, state) {
        if (state.loadingStatus == LoadingStatus.success) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Successfully deleted staff'),
          ));
          context.pop();
        }
      },
      child: Column(
        children: [
          CustomAppBar(
            title: 'Staff Profile',
            actions: [
              IconButton(
                onPressed: () {
                  _staffBloc.add(DeleteStaff(staffId: widget.staff.id));
                },
                icon: const Icon(Icons.delete_rounded),
              )
            ],
          ),
          Expanded(
            child: ListView(
              padding: const EdgeInsets.all(24),
              children: [
                ListTile(
                  isThreeLine: true,
                  leading: Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(
                      image: widget.staff.profilePictureUrl != null
                          ? DecorationImage(
                              image: NetworkImage(
                                widget.staff.profilePictureUrl!,
                              ),
                              fit: BoxFit.cover,
                            )
                          : null,
                      gradient: accentGradient,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: widget.staff.profilePictureUrl == null
                        ? const Icon(Icons.person_rounded)
                        : null,
                  ),
                  title: Text(
                    '${widget.staff.firstName} ${widget.staff.lastName}'
                        .toUpperCase(),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Divider(),
                      if (widget.staff.userType == 'driver')
                        TextScroll(
                          'Email: ${widget.staff.email}',
                          velocity: const Velocity(
                            pixelsPerSecond: Offset(16, 0),
                          ),
                        ),
                      const SizedBox(height: 2),
                      if (widget.staff.userType == 'driver')
                        Text('Phone Number: +${widget.staff.phoneNumber}'),
                      const SizedBox(height: 2),
                      Text('Type: ${widget.staff.userType}'.toUpperCase()),
                    ],
                  ),
                ),
                const SizedBox(height: 16),
                if (widget.staff.userType == 'driver')
                  const Text(
                    'I.D Image (Front)',
                    textAlign: TextAlign.center,
                  ),
                const SizedBox(height: 8),
                if (widget.staff.userType == 'driver')
                  AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            widget.staff.idFrontImageUrl!,
                          ),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(16),
                      ),
                    ),
                  ),
                const SizedBox(height: 16),
                if (widget.staff.userType == 'driver')
                  const Text(
                    'I.D Image (Back)',
                    textAlign: TextAlign.center,
                  ),
                const SizedBox(height: 8),
                if (widget.staff.userType == 'driver')
                  AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            widget.staff.idBackImageUrl!,
                          ),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(16),
                      ),
                    ),
                  ),
                const SizedBox(height: 16),
                if (widget.staff.userType == 'driver')
                  const Text(
                    'Valid License Image',
                    textAlign: TextAlign.center,
                  ),
                const SizedBox(height: 8),
                if (widget.staff.userType == 'driver')
                  AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            widget.staff.licenseImageUrl!,
                          ),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(16),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
