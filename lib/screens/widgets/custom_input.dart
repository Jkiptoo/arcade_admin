import 'package:provider_app/extensions.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class CustomInput extends StatefulWidget {
  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final String? defaultValue;
  final TextInputType? textInputType;
  final void Function(String value)? onChanged;
  final bool readOnly;

  const CustomInput({
    super.key,
    this.controller,
    this.labelText,
    this.hintText,
    this.defaultValue,
    this.textInputType,
    this.onChanged,
    this.readOnly = false,
  });

  @override
  State<CustomInput> createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  bool _isVisible = false;
  @override
  Widget build(BuildContext context) {
    if (widget.textInputType == TextInputType.phone) {
      return IntlPhoneField(
        showCountryFlag: false,
        initialCountryCode: 'KE',
        showDropdownIcon: false,
        decoration: InputDecoration(
          hintText: widget.hintText?.capitalize(),
          labelText: widget.labelText?.capitalize(),
        ),
        countries: const ['KE'],
        controller: widget.controller,
        disableLengthCheck: true,
        flagsButtonPadding: const EdgeInsets.only(left: 24),
        keyboardType: TextInputType.phone,
        validator: (phoneNumber) => _validator(phoneNumber?.completeNumber),
      );
    }
    return TextFormField(
      initialValue: widget.defaultValue,
      controller: widget.controller,
      cursorColor: const Color.fromRGBO(52, 198, 173, 1),
      keyboardType: widget.textInputType,
      obscureText: widget.textInputType == TextInputType.visiblePassword &&
          _isVisible == false,
      decoration: InputDecoration(
        hintText: widget.hintText?.capitalize(),
        labelText: widget.labelText?.capitalize(),
        suffixIcon: widget.textInputType == TextInputType.visiblePassword
            ? GestureDetector(
                onTap: () => setState(() => _isVisible = !_isVisible),
                child: Icon(
                  _isVisible
                      ? Icons.visibility_rounded
                      : Icons.visibility_off_rounded,
                ),
              )
            : null,
      ),
      validator: _validator,
      onChanged: widget.onChanged,
      readOnly: widget.readOnly,
    );
  }

  String? _validator(String? value) {
    if (value == null || value.isEmpty) return 'This field is required';
    if (widget.textInputType == TextInputType.emailAddress) {
      if (!EmailValidator.validate(value)) return 'Invalid email address';
    }
    if (widget.textInputType == TextInputType.phone) {
      if (value.substring(1).length != 12) {
        return 'Please enter a valid phone number';
      }
    }
    if (widget.textInputType == TextInputType.visiblePassword) {
      if (value.length < 8) return 'Please enter more than 8 characters';
    }
    return null;
  }
}
