import 'package:flutter/material.dart';
import 'package:provider_app/configs/theme.dart';

class GradientButton extends StatelessWidget {
  final String title;
  final Widget? icon;
  final VoidCallback? onTap;
  final bool isOutlined;
  final bool isLoading;

  const GradientButton({
    super.key,
    required this.title,
    this.icon,
    this.onTap,
    this.isOutlined = false,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(maxHeight: 52),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(48),
        gradient: isOutlined ? null : accentGradient,
        border: isOutlined ? Border.all(color: accent, width: 2) : null,
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(48),
          onTap: onTap,
          splashColor: Colors.white.withOpacity(0.2),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (icon != null) icon!,
                if (icon != null) const SizedBox(width: 8),
                if (isLoading)
                  const Center(
                    child: SizedBox.square(
                      dimension: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 4,
                        color: Colors.white,
                      ),
                    ),
                  )
                else
                  Flexible(
                    child: Text(
                      title.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
