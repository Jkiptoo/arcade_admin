import 'package:provider_app/configs/theme.dart';
import 'package:flutter/material.dart';

class GradientIcon extends StatelessWidget {
  final IconData icon;

  const GradientIcon(this.icon, {super.key});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) => accentGradient.createShader(bounds),
      child: Icon(icon),
    );
  }
}
